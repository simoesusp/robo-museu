Projeto de controle dos motores do Robô-Museu usando o ESP32 e a Ponte H MonsterMoto(r)Shield.

**Observações:**
Não adianta conectar no pino 3,3V da Ponte H, ele  leva de nada a lugar nenhum.
Quando conectamos os 3,3V que saiam do ESP na entrada de 5v funcionou, mas não achamos que deveria funcionar na Documentação  da Ponte H.

**Pinagem:**
ESP - PONTE H
21  - 7
19  - 4
18  - 8
5   - 9
23  - 5
22  - 6
