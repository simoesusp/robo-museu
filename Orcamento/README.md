# Orçamento

Os preços abaixo estão sem o frete, em média o frete para cada item varia entre R$10,00 e R$20,00 - Recomenda-se entrar em contato com o vendedor para tentar comprar a maioria dos itens da mesma loja, para reduzir o frete.

### Robô Museu com Ponte H
|   Nome do componente   | Preço (sem frete) | Link Mercadolivre  |
| ---------------------- | ----------------- | ----------------- |
|  2x Motor Bosch Cep 9 390 453 023 12v 26/43rpm  | R$385,0           | [Link](https://produto.mercadolivre.com.br/MLB-965194319-motor-bosch-cep-9-390-453-023-12v-2643rpm-_JM#position=2&type=item&tracking_id=b3e32fad-3980-413c-aab6-9f39dc9131d9) |
|  Monster Moto Shield VNH3ASP30 5.5 a 16VDC 30A  | R$119,61           | [Link](https://www.usinainfo.com.br/shields-para-arduino/monster-moto-shield-ponte-h-dupla-vnh3asp30-55-a-16vdc-30a-4536.html) |
|  Esp32 Wifi + Bluetooth | R$52,0           | [Link](https://produto.mercadolivre.com.br/MLB-942296004-esp32-_JM#position=2&search_layout=grid&type=item&tracking_id=b0597834-e058-46cc-8aa4-57a6c0f00061) |
|  6x Sensor Sonar Hc Sr-04p       | R$18,96           | [Link](https://produto.mercadolivre.com.br/MLB-795588540-modulo-sensor-sonar-distancia-pra-arduino-nodemcu-esp8266-_JM#position=15&type=item&tracking_id=41e80598-9eb1-428f-b68c-457e7ddbcdeb) |
|  2xBaterias de 12V Tipo Nobreak Modelo 12V 7.0AH 04F001 - UP 1270 SEG       | R$110,96           | [Link](https://produto.mercadolivre.com.br/MLB-1227531447-bateria-selada-unicoba-unipower-12v-70ah-up1270seg-bateria-p-no-break-_JM#position=2&search_layout=stack&type=item&tracking_id=838e84a1-acf9-4417-be22-66c082f2f4d4) |
|  2x Rodas Eva Aro 12 Bicicleta Infantil       | R$46,00           | [Link](https://produto.mercadolivre.com.br/MLB-1504287278-kit-2-rodas-eva-aro-12-bicicleta-infantil-_JM#position=4&search_layout=stack&type=item&tracking_id=380c872c-e778-4be4-975d-41d7b8442463) |
|  Chapa Aluminio Lisa 1100h14 3mm - 700x700mm       | R$300,96           | [Link](https://produto.mercadolivre.com.br/MLB-1427952264-chapa-aluminio-lisa-1100h14-3mm-700x700mm-frete-gratis-_JM#position=1&search_layout=stack&type=item&tracking_id=a2fb018b-809b-42a2-86fa-9b0e517387a2) |
|  Micro Ventilador 80x80x25 Gc Fan Cooler 12v 0,15a 80mm       | R$12,96           | [Link](https://produto.mercadolivre.com.br/MLB-1692532091-micro-ventilador-80x80x25-gc-fan-cooler-12v-015a-80mm-_JM?searchVariation=76100778667#searchVariation=76100778667&position=28&search_layout=grid&type=item&tracking_id=64dd690d-284a-429d-814b-d9b0b6f8630c) |




### Robô Museu com IRFZ44n - Sem marcha à Ré

|   Nome do componente   | Preço (sem frete) | Link para compra  |
| ---------------------- | ----------------- | ----------------- |
|  10x IRFZ44n       | R$8,90           | [Link](https://produto.mercadolivre.com.br/MLB-1618714569-irfz44n-ir-fz-44-n-transistor-original-_JM?matt_tool=87716990&matt_word=&matt_source=google&matt_campaign_id=12413740998&matt_ad_group_id=119070072438&matt_match_type=&matt_network=g&matt_device=c&matt_creative=500702333978&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=348086178&matt_product_id=MLB1618714569&matt_product_partition_id=337120033364&matt_target_id=pla-337120033364&gclid=Cj0KCQjw1a6EBhC0ARIsAOiTkrGxbfB0aB0DPjqvSGa3ACYptAqYQLcZ2k7zoyrh-q9QZUiHjGjjUwYaAuS8EALw_wcB) |

### Controladora Raspberry

|   Nome do componente   | Preço (sem frete) | Link para compra  |
| ---------------------- | ----------------- | ----------------- |
|  Raspberry Pi 4 Pi4 Model B V1.2 2gb C/ Nf   | R$459,90           | [Link](https://produto.mercadolivre.com.br/MLB-1630275706-raspberry-pi-4-pi4-model-b-v12-2gb-c-nf-pronta-entrega-_JM#position=4&search_layout=grid&type=item&tracking_id=612b9ea1-3790-4c75-9bbb-fe7fa919e338) |





