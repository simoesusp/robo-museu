<div align="center"> <h2>Robô Museu</h2> </div></p>

<p><div align="justify"> Diante de um cenário de pandemia e escassez de recursos, o maior desafio na administração do Museu de Computação é a manutenção do seu funcionamento. O Museu precisou repensar seu papel diante do distanciamento social. A primeira iniciativa nesta linha foi a organização de uma exposição online, chamada "Alunos que dão voz a máquinas". A exposição foi uma iniciativa do Museu em conjunto com estudantes que ingressaram em 2020 no curso de Bacharelado em Sistemas de Informação e cursaram a disciplina Evolução Histórica da Computação. Agora, o objetivo é ampliar o acesso remoto a quem deseja visitar o museu de forma mais interativa Assim, o presente projeto visa habilitar o Museu com recursos tecnológicos que permitam aos usuários interagir com as peças do museu de forma remota. Um total de quatro ideias principais será implementado neste projeto: novas edições de exposições virtuais, acesso remoto via imagem panorâmica do Museu, controle remoto de câmeras dispostas no Museu, por um período de tempo, e controle de um robô que percorrerá o Museu filmando as peças.</div></p>

### Video Ensinando a construir o Robô do Museu 
- https://drive.google.com/file/d/15RGylFXFAKK1gVILRtHE3O8WvPF_ed0A/view?usp=sharing

<p><div align="justify"> O projeto 3D do robô que será construído pode ser acessado <a href="https://a360.co/3rbcE0v">aqui</a>. </div></p>

<br>

<div align="center">
    <figure>
        <img src="./FotoRobo.jpeg" width="395.5" height="822.5">
        <figcaption>Imagem do projeto 3D do robô. </figcaption>
    </figure>
</div>
