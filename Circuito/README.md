# Circuito do Robô

## Controlador ESP32

- O Microcontrolador selecionado foi o ESP32 - https://produto.mercadolivre.com.br/MLB-942296004-esp32-_JM#position=2&search_layout=grid&type=item&tracking_id=b0597834-e058-46cc-8aa4-57a6c0f00061

<div align="center">
 <img src="./ESP32_PINs.jpg" width="300">
</div>


## Versão com a Ponte H Monster Motor Vnh3sp30 30a 2 Canais

- A ponte H selecionada foi a Monster Motor Vnh3sp30 30a 2 Canais - https://produto.mercadolivre.com.br/MLB-1719187247-ponte-h-monster-motor-vnh3sp30-30a-2-canais-para-arduino-_JM#position=14&search_layout=grid&type=item&tracking_id=4236cfb5-2e0c-4547-b39e-4e92dacdde20
- NÃO ESQUEÇA de colocar um dissipador ou um ventilador sobre a Ponte H Monster, pois vai aquecer muito com os motores de limpador de parabrisa!!
- WARNING: É possível ligar 12v direto no VIN do ESP32, mas vários módulos meus já queimaram assim!!
- Se mudar o regulador MC-7833 por outro, cuidado para que a tensão de entrada máxima seja superior a 12v da bateris (que as vezes quando bem carregada chega a 13,5v) 
  - Pode usar o Regulador LD1117V33 - Tensão 3,3 V - 0.8 A

<div align="center">
 <img src="./Ponte_H_Monster_Motor_Vnh3sp30_30a_2_Canais.jpg" width="300">
</div>

## Circuito do Robô

- O circuito consiste em conectar o ESP32 já alimentado por um regulador de tensão de 3.3V à Ponte h

<div align="center">
 <img src="./ESP32_LM7833.png" width="300">
</div>

- Observações: Não adianta conectar no pino 3,3V da Ponte H, ele  leva de nada a lugar nenhum.
Quando conectamos os 3,3V que saiam do ESP na entrada de 5v funcionou, mas não achamos que deveria funcionar na Documentação  da Ponte H.

<div align="center">
 <img src="./H_Bridge_Monster_ESP32.jpg" width="300">
</div>

## Alternativa sem Ponte H - Sem marcha a Ré

- É possível economizar substituindo a ponte H por MOSFETs tipo IRFZ44n, mas o robô só poderá se movimentar para frente.
- O IRFZ44n aguenta 49A, ou seja, muito mais do que os motores necessitam. Portanto podem ser substituídos por MOSFETs mais baratos, capazes de suportar menor corrente. Cerca de 10A já será adequando para os motores Bosch selecionados - https://storage.googleapis.com/baudaeletronicadatasheet/IRFZ44N.pdf

<div align="center">
 <img src="./ESP32_LM7833_IRFZ44n.png" width="300">
</div>
