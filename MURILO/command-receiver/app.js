//const http = require("http");
const https = require('https');
const fs = require('fs');

//const server = http.createServer(app);
var options = {
  key: fs.readFileSync('private.key'),
  cert: fs.readFileSync('certificate.crt')
};

const url = require('url');

https.createServer(options, function (req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Request-Method', '*');
  res.setHeader ('Access-Control-Allow-Credentials', true);
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
  const queryObject = url.parse(req.url,true).query;
  const urlParsed = url.parse(req.url, true).pathname;
  console.log(queryObject);
  console.log(urlParsed);

  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end('Feel free to add query parameters to the end of the url');
}).listen(8080);
