const express = require("express");
const app = express();

let broadcaster;
const port = 4000;

//const http = require("http");
const https = require('https');
const fs = require('fs');

//const server = http.createServer(app);
var server = https.createServer({ 
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem') 
}, app);


const io = require("socket.io")(server);
app.use(express.static(__dirname + "/public"));

io.sockets.on("error", e => console.log(e));
io.sockets.on("connection", socket => {
  console.log("New connection");
  socket.on("broadcaster", () => {
    console.log("New broadcaster");
    broadcaster = socket.id;
    socket.broadcast.emit("broadcaster");
  });
  socket.on("watcher", () => {
    console.log("Watcher trying to connect");
    socket.to(broadcaster).emit("watcher", socket.id);
  });
  socket.on("offer", (id, message) => {
    console.log("Offer");
    socket.to(id).emit("offer", socket.id, message);
  });
  socket.on("answer", (id, message) => {
    console.log("Answer");
    socket.to(id).emit("answer", socket.id, message);
  });
  socket.on("candidate", (id, message) => {
    console.log("Candidate");
    socket.to(id).emit("candidate", socket.id, message);
  });
  socket.on("disconnect", () => {
    console.log("User disconnected");
    socket.to(broadcaster).emit("disconnectPeer", socket.id);
  });
  socket.on("endCall", (user) => {
    console.log( user + " ended the call");
    socket.to(broadcaster).emit("disconnectPeer", socket.id);
  });
  socket.on("mainWatcher", (id, message) => {
    console.log("new main watcher");
    socket.to(id).emit("mainWatcher", socket.id, message);
  });
});
server.listen(port, () => console.log(`Server is running on port ${port}`));
