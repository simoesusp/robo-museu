const https = require('https');
const peerConnections = {};
var numberOfPeersConnected = 0;
const receiveChannels = {};
const config = {
  iceServers: [
    { 
      "urls": "stun:stun.l.google.com:19302",
    },
    { 
      "urls": "turn:numb.viagenie.ca",
      "username": "webrtc@live.com",
      "credential": "muazkh"
    }
  ]
};
var mainWatcher = '00'
// Parâmetros para repassar os comandos para o robô como IP local e porta
const reqOptions = {
  hostname: 'localhost',
  port: 8080,
  path: '/test',
  method: 'GET'
}

//const socket = io.connect(window.location.origin, {secure: true});
const socket = io.connect('wss://principia.icmc.usp.br', {secure: true}, {transports: ['websocket']});

var statusText = null;
var incomingMessages = null;
var videoElement = null;
var audioSelect = null;
var videoSelect = null;
var robotIPInput = null;
var robotPortInput = null;
var manualCommandInput = null;
var sendCommandButton = null;

window.onload = (event) => {
  console.log('page loaded')
  startup();
}
window.onunload = window.onbeforeunload = () => {
  console.log('page unloading')
  socket.close();
};

function startup() {
  console.log('starting...');
  // Get page Elements
  robotIPInput = document.querySelector("#robotIP");
  robotPortInput = document.querySelector("#robotPort");
  manualCommandInput = document.querySelector("#manualCommand");
  sendCommandButton = document.querySelector("button#sendCommandButton");
  videoElement = document.querySelector("video");
  audioSelect = document.querySelector("select#audioSource");
  videoSelect = document.querySelector("select#videoSource");
  statusText = document.querySelector("#status");
  incomingMessages = document.querySelector('#messages');

  // Get audio and video
  audioSelect.onchange = getStream;
  videoSelect.onchange = getStream;
  getStream()
  .then(getDevices)
  .then(gotDevices);
  console.log('page started');
  updateStatusText(0);

  // Set Event Listeners
  sendCommandButton.addEventListener("click", sendManualCommand, false);
}

socket.on("broadcaster", data => {
  console.log("New broadcaster socket connection");
});

socket.on("answer", (id, description) => {
  console.log("Answer received");
  peerConnections[id].setRemoteDescription(description);
  if (mainWatcher == '00')
  {
    mainWatcher = id;
    socket.emit("mainWatcher", id, "true");
  }
  updateStatusText(1);
});

socket.on("watcher", id => {
  console.log("New watcher trying to connect")
  const peerConnection = new RTCPeerConnection(config);
  const receiveChannel = peerConnection.createDataChannel('channel'+id);
  receiveChannel.onerror = (error) => {
    console.log("Data Channel Error:", error);
  };
  //peerConnection.ondatachannel = receiveChannelCallback;
  peerConnections[id] = peerConnection;
  receiveChannels[id] = receiveChannel;

  let stream = videoElement.srcObject;
  stream.getTracks().forEach(track => peerConnection.addTrack(track, stream));

  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      socket.emit("candidate", id, event.candidate);
    }
  };

  peerConnection
    .createOffer()
    .then(sdp => peerConnection.setLocalDescription(sdp))
    .then(() => {
      socket.emit("offer", id, peerConnection.localDescription);
    });

  receiveChannel.onmessage = handleReceiveMessage;
  receiveChannel.onopen = handleReceiveChannelOpen(id);
  receiveChannel.onclose = handleReceiveChannelClose;
});

socket.on("candidate", (id, candidate) => {
  peerConnections[id].addIceCandidate(new RTCIceCandidate(candidate));
});

socket.on("disconnectPeer", id => {
  if (peerConnections[id])
  {
    if (receiveChannels[id])
    {
      receiveChannels[id].close();
      delete receiveChannels[id];
    }
    peerConnections[id].close();
    delete peerConnections[id];
    if (id == mainWatcher)
    mainWatcher = '00';
    updateStatusText(-1);
  }
});


function getDevices() {
  return navigator.mediaDevices.enumerateDevices();
}

function gotDevices(deviceInfos) {
  window.deviceInfos = deviceInfos;
  for (const deviceInfo of deviceInfos) {
    const option = document.createElement("option");
    option.value = deviceInfo.deviceId;
    if (deviceInfo.kind === "audioinput") {
      option.text = deviceInfo.label || `Microphone ${audioSelect.length + 1}`;
      audioSelect.appendChild(option);
    } else if (deviceInfo.kind === "videoinput") {
      option.text = deviceInfo.label || `Camera ${videoSelect.length + 1}`;
      videoSelect.appendChild(option);
    }
  }
}

function getStream() {
  if (window.stream) {
    window.stream.getTracks().forEach(track => {
      track.stop();
    });
  }
  const audioSource = audioSelect.value;
  const videoSource = videoSelect.value;
  const constraints = {
    audio: { deviceId: audioSource ? { exact: audioSource } : undefined },
    video: { deviceId: videoSource ? { exact: videoSource } : undefined }
  };
  return navigator.mediaDevices
    .getUserMedia(constraints)
    .then(gotStream)
    .catch(handleError);
}

function gotStream(stream) {
  window.stream = stream;
  audioSelect.selectedIndex = [...audioSelect.options].findIndex(
    option => option.text === stream.getAudioTracks()[0].label
  );
  videoSelect.selectedIndex = [...videoSelect.options].findIndex(
    option => option.text === stream.getVideoTracks()[0].label
  );
  videoElement.srcObject = stream;
  socket.emit("broadcaster");
}

function handleError(error) {
  console.error("Error: ", error);
}

function updateStatusText(dif) {
  numberOfPeersConnected += dif;
  if (statusText)
  {
    console.log("alterado status");
    statusText.textContent = "Conectado com " + numberOfPeersConnected + " usuários / main watcher id= " + mainWatcher;
  }
  else
    console.log("erro de status");
}

function receiveChannelCallback(event) {
  console.log('openning data channel');
  receiveChannel = event.channel;
  
}

function handleReceiveMessage(event) {
  const message = event.data;
  var el = document.createElement("p");
  var txtNode = document.createTextNode(message);

  el.appendChild(txtNode);
  incomingMessages.appendChild(el);

  switch(message) {
    case 'parar-completo':
      sendCommand('/motor?speed=0');
      sendCommand('/direita?speed=0');
      sendCommand('/esquerda?speed=0');
      break;
    case 'andar-frente':
      sendCommand('/motor?speed=25');
      break;
    case 'andar-tras':
      sendCommand('/motor?speed=-25');
      break;
    case 'virar-direita':
      sendCommand('/esquerda?speed=10');
      sendCommand('/direita?speed=-10');
      break;
    case 'virar-esquerda':
      sendCommand('/direita?speed=10');
      sendCommand('/esquerda?speed=-10');
      break;
    default:
      console.log("Comando '"+message+"' não reconhecido");
  }
}

// function handleReceiveChannelStatusChange(event) {
//   if (receiveChannel) {
//     console.log("Receive channel's status has changed to " +
//                 receiveChannel.readyState);
//   }
// }

function handleReceiveChannelOpen(id) {
  console.log("Receive channel open id="+id);
}

function handleReceiveChannelClose(event) {
  console.log("Receive channel closed"); 
}

function sendManualCommand() {
  if(robotIPInput.value)
    reqOptions.hostname = robotIPInput.value;
  else 
    console.log("Digite o IP local do robô");

  if(robotPortInput.value)
    reqOptions.port = robotPortInput.value;
  else
    console.log("Digite a porta correta para mandar os comandos");

  if(!manualCommandInput.value)
    reqOptions.path = "/ligaled";
  else
    reqOptions.path = manualCommandInput.value;

  const req = https.request(reqOptions, res => {
    console.log(`statusCode: ${res.statusCode}`)
  
    res.on('data', d => {
      console.log(d)
    })
  })
  
  req.on('error', error => {
    console.error(error)
  })
  
  req.end()
}

function sendCommand(path) {
  reqOptions.path = path;

  const req = https.request(reqOptions, res => {
    //console.log(`statusCode: ${res.statusCode}`)
  
    res.on('data', d => {
      console.log(d)
    })
  })
  
  req.on('error', error => {
    console.error(error)
  })
  
  req.end()
}