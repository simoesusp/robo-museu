var peerConnection = null;
var sendChannel = null;
const config = {
  iceServers: [
      { 
        "urls": "stun:stun.l.google.com:19302",
      },
      // { 
      //   "urls": "turn:TURN_IP?transport=tcp",
      //   "username": "TURN_USERNAME",
      //   "credential": "TURN_CREDENTIALS"
      // }
  ]
};
var video = null;
var audioButton = null;
var connectButton = null;
var sendButton = null;
var messageBox = null;
var connected = false;
var mainWatcher = false;


// const socket = io.connect(window.location.origin, {secure: true});
const socket = io.connect('wss://principia.icmc.usp.br', {secure: true}, {transports: ['websocket']});

window.onload = (event) => {
  console.log('page loaded');
  startup();
}
window.onunload = window.onbeforeunload = () => {
  console.log('page unloading');
  changeConnectState(false);
  socket.close();
};

function startup() {
  console.log('page starting..');

  // Get page Elements
  video = document.querySelector("video");
  audioButton = document.querySelector("#audio");
  connectButton = document.querySelector("#connect");
  sendButton = document.querySelector("#sendMessage");
  messageBox = document.querySelector('#yourMessage');

  controlPanel = document.querySelector("#controlPanel");
  forwardButton = document.querySelector("#forward");
  reverseButton = document.querySelector("#reverse");
  rightButton = document.querySelector("#right");
  leftButton = document.querySelector("#left");
  bodyWrapper = document.querySelector("div#wrapper");

  // Set Event Listeners
  audioButton.addEventListener("click", handleAudioChange, false);
  connectButton.addEventListener("click", connect, false);
  sendButton.addEventListener("click", sendMessage, false);
  // Comandos
  forwardButton.addEventListener("mousedown", sendForwardCommand, false);
  forwardButton.addEventListener("mouseup", sendStopCommand, false);
  reverseButton.addEventListener("mousedown", sendReverseCommand, false);
  reverseButton.addEventListener("mouseup", sendStopCommand, false);
  rightButton.addEventListener("mousedown", sendRightCommand, false);
  rightButton.addEventListener("mouseup", sendStopCommand, false);
  leftButton.addEventListener("mousedown", sendLeftCommand, false);
  leftButton.addEventListener("mouseup", sendStopCommand, false);
  bodyWrapper.addEventListener("mouseleave", sendStopCommand, false);

  console.log('page started');
}

socket.on("connect_error", (err) => {  
  console.log(`connect_error due to ${err.message}`);
});

socket.on("mainWatcher", (id, message) => {
  if (message === "true")
    changeWatcherState(true);
  else
    changeWatcherState(false);
})

socket.on("offer", (id, description) => {
  peerConnection = new RTCPeerConnection(config);
  //sendChannel = peerConnection.createDataChannel('sendchannel');
  peerConnection.ondatachannel = receiveChannelCallback;
  

  peerConnection
    .setRemoteDescription(description)
    .then(() => peerConnection.createAnswer())
    .then(sdp => peerConnection.setLocalDescription(sdp))
    .then(() => {
      socket.emit("answer", id, peerConnection.localDescription);
    });

  peerConnection.ontrack = event => {
    video.hidden = false;
    video.srcObject = event.streams[0];
  };

  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      socket.emit("candidate", id, event.candidate);
    }
  };
});

function handleSendChannelStatusChange(event) {
  console.log('channel state change')
  if (sendChannel) {
    var state = sendChannel.readyState;

    if (state === "open") {
      console.log('Channel open');
      changeConnectState(true);
    } else {
      connected = false;
      console.log('Channel closed');
      changeConnectState(false);
    }
  }
}

function changeConnectState(newState) {
  connected = newState;
  if (newState)
  {
    //sendButton.disabled = false;
    //controlPanel.hidden = false;
    audioButton.disabled = false;
    connectButton.innerHTML = "Desconectar";
    video.hidden = false;
  }
  else 
  {
    console.log("Disconnecting..")

    if (peerConnection)
    {
      if (sendChannel.readyState === "open")
        sendChannel.close();
      peerConnection.close();
      console.log("Disconnected")
    }
    else
      console.log("Connection not found");

    sendChannel = null;
    peerConnection = null;

    socket.emit("endCall", "watcher");

    sendButton.disabled = true;
    audioButton.disabled = true;
    controlPanel.hidden = true;
    connectButton.innerHTML = "Conectar";
    video.hidden = true;
  }
}

function changeWatcherState(main) {
  if (main)
  {
    mainWatcher = true;
    sendButton.disabled = false;
    controlPanel.hidden = false;
    console.log("You are the Main Watcher");
  }
  else 
  {
    mainWatcher = false;
    sendButton.disabled = true;
    controlPanel.hidden = true;
  }
}

socket.on("candidate", (id, candidate) => {
  peerConnection
    .addIceCandidate(new RTCIceCandidate(candidate))
    .catch(e => console.error(e));
});

socket.on("connect", () => {
  //newUser
  console.log("New socket connection");
});

socket.on("broadcaster", () => {
  //robotOnline
  console.log("New broadcaster socket connection");
});



function enableAudio() {
  console.log("Enabling audio")
  video.muted = false;
}

function handleAudioChange() {
  if (video.muted) {
    console.log("Enabling audio")
    video.muted = false;
    audioButton.innerHTML = "Desativar Audio"
  } else {
    console.log("Disabling audio")
    video.muted = true;
    audioButton.innerHTML = "Ativar Audio"
  }
  
}

function connect() {
  if (connected)
  {
    changeConnectState(false);
    changeWatcherState(false);
  }
  else
  {
    console.log("Connecting..")
    socket.emit("watcher");
  }
  
}

function receiveChannelCallback(event) {
  console.log('openning data channel');
  sendChannel = event.channel;
  sendChannel.onmessage = handleReceiveMessage;
  sendChannel.onopen = handleSendChannelStatusChange;
  sendChannel.onclose = handleSendChannelStatusChange;
  sendChannel.onerror = (error) => {
    console.log("Data Channel Error:", error);
  };
}

function handleReceiveMessage(event) {
  const message = event.data;
  console.log = ("New message: "+ message);
}

function sendMessage() {
  if (sendChannel && mainWatcher)
  {
    const message = messageBox.value;
    sendChannel.send(message);
    messageBox.value = "";
    messageBox.focus();
  }
}

function sendForwardCommand() {
  if (sendChannel && mainWatcher)
  {
    const message = "andar-frente"
    sendChannel.send(message);
  }
}

function sendStopCommand() {
  if (sendChannel && mainWatcher)
  {
    const message = "parar-completo"
    sendChannel.send(message);
  }
}

function sendReverseCommand() {
  if (sendChannel && mainWatcher)
  {
    const message = "andar-tras"
    sendChannel.send(message);
  }
}

function sendRightCommand() {
  if (sendChannel && mainWatcher)
  {
    const message = "virar-direita"
    sendChannel.send(message);
  }
}

function sendLeftCommand() {
  if (sendChannel && mainWatcher)
  {
    const message = "virar-esquerda"
    sendChannel.send(message);
  }
}